from django.db import models
from django.urls import reverse
from django.utils.text import slugify


# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, unique=True, blank=True)
    image = models.ImageField(upload_to='category/%Y/%m/%d', blank=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'
    
    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        else:
            self.slug = slugify(self.name)
        return super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('shop:product_list_by_category', args=[self.slug])


class Product(models.Model):
    category = models.ForeignKey(Category,
                                 related_name='products',
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, blank=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
    
    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        else:
            self.slug = slugify(self.name)
        return super(Product, self).save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('shop:product_detail', args=[self.id, self.slug])
        

class Promotion(models.Model):
    header = models.CharField(max_length=55, db_index=True)
    slug = models.SlugField(max_length=20, db_index=True, blank=True)
    subheader = models.CharField(max_length=55)
    image = models.ImageField(upload_to='promo/%Y/%m/%d', blank=True)
    text = models.TextField(blank=True)
    available = models.BooleanField(default=False)

    class Meta:
        ordering = ('header',)
        verbose_name = 'promo'
        verbose_name_plural = "promo's"
        index_together = (('id', 'slug'),)
    
    def __str__(self):
        return self.header

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.header)
        else:
            self.slug = slugify(self.header)
        return super(Promotion, self).save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('shop:promo_detail', args=[self.id, self.slug])
        