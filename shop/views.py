from django.shortcuts import render, get_object_or_404
from .models import Category, Product, Promotion
from cart.forms import CartAddProductForm
from django.views import generic
from django.urls import reverse_lazy, reverse
from accounts.models import CustomUser
from django.http import request



##############> Main pages' views <###############

def home(request, category_slug=None):
    categories = Category.objects.all()
    products = Product.objects.all()
    promos = Promotion.objects.all()
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'shop/home.html', {'categories':categories, 'products':products, 'promos':promos})


def about(request):
    return render(request, 'shop/about.html')


def contact(request):
    return render(request, 'shop/contact.html')



#####################> Product <###################

def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.all()
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'shop/product/shop.html',
                  {'category':category,
                  'categories':categories,
                  'products':products })


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm 
    return render(request, 'shop/product/shop-single.html', 
                 {'product':product, 
                 'cart_product_form':cart_product_form })
    

class ProductCreate(generic.CreateView):
    model = Product
    fields = ['category', 'name', 'image', 'description', 'price', 'available']
    # success_url = reverse_lazy('shop:product_list')
    template_name = 'shop/product/create.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:product_list'))


class ProductDelete(generic.DeleteView):
    model = Product
    # if request == 'admin_panel':
    #     success_url = reverse_lazy('admin_panel')
    # else:
    #     success_url = reverse_lazy('shop:home')
    template_name = 'shop/category/delete.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))


class ProductUpdate(generic.UpdateView):
    model = Product
    fields = ['name', 'image', 'description', 'price', 'available']
    success_url = reverse_lazy('shop:product_list')
    template_name = 'shop/product/update.html'

###################> Categories <#################

class CategoryCreate(generic.CreateView):
    model = Category
    fields = ['name', 'slug', 'image']
    # success_url = reverse_lazy('shop:product_list')
    template_name = 'shop/category/create.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:product_list'))


class CategoryDelete(generic.DeleteView):
    model = Category
    # if request == 'admin_panel':
    #     success_url = reverse_lazy('admin_panel')
    # else:
    #     success_url = reverse_lazy('shop:home')
    template_name = 'shop/category/delete.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))


class CategoryUpdate(generic.UpdateView):
    model = Category
    fields = ['name', 'image']
    # success_url = reverse_lazy('shop:home')
    template_name = 'shop/category/update.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))

#####################> Promos <#######################

class PromoCreate(generic.CreateView):
    model = Promotion
    fields = ['header', 'subheader', 'image', 'text', 'available']
    # success_url = reverse_lazy('shop:home')
    template_name = 'shop/promo/create.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))


class PromoDelete(generic.DeleteView):
    model = Promotion
    # if request == 'admin_panel':
    #     success_url = reverse_lazy('admin_panel')
    # else:
    #     success_url = reverse_lazy('shop:home')
    template_name = 'shop/promo/delete.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))


class PromoUpdate(generic.UpdateView):
    model = Promotion
    fields = ['header', 'subheader', 'image', 'text', 'available']
    # success_url = reverse_lazy('shop:home')
    template_name = 'shop/promo/update.html'

    def get_success_url(self):
        return self.request.GET.get('next', reverse('shop:home'))


class PromoDetail(generic.DetailView):
    model = Promotion
    context_object_name = 'promo'
    template_name = 'shop/promo/detail.html'
