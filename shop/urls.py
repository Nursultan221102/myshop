from django.urls import path
from . import views
app_name = 'shop'
urlpatterns = [
    path('', views.home, name='home'),
    path('new-category/', views.CategoryCreate.as_view(), name='category_create'),
    path('new-product/', views.ProductCreate.as_view(), name='product_create'),
    path('delete-category/<int:pk>/', views.CategoryDelete.as_view(), name='category_delete'),
    path('update-category/<int:pk>/', views.CategoryUpdate.as_view(), name='category_update'),
    path('delete-product/<int:pk>/', views.ProductDelete.as_view(), name='product_delete'),
    path('update-product/<int:pk>/', views.ProductUpdate.as_view(), name='product_update'),
    path('new-promo/', views.PromoCreate.as_view(), name='promo_create'),
    path('delete-promo/<int:pk>/', views.PromoDelete.as_view(), name='promo_delete'),
    path('update-promo/<int:pk>/', views.PromoUpdate.as_view(), name='promo_update'),
    path('detail-promo/<int:pk>/<slug:slug>/', views.PromoDetail.as_view(), name='promo_detail'),
    path('shop', views.product_list, name='product_list'),
    path('<slug:category_slug>/', views.product_list, name='product_list_by_category'),
    path('<int:id>/<slug:slug>/', views.product_detail, name='product_detail'),
    
    
    path('about', views.about, name='about'),
    path('contact', views.contact, name='contact'),
]
