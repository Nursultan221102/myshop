from django.urls import path 
from .views import SignUpView, UserListView, UserPersonalRoom, AdminPanelView


urlpatterns = [
    path('', UserListView.as_view(), name='users_list'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('my-room/', UserPersonalRoom.as_view(), name='p_room'),
    path('admin-panel/', AdminPanelView.as_view(), name='admin_panel'),
]