from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

GENDER = (
    ('MALE', 'male'),
    ('FEMALE', 'female')
)


class CustomUser(AbstractUser):
    gender = models.CharField(choices=GENDER, max_length=55, blank=True, null=True)
    birthdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['username']

    def __str__(self):
        return self.username