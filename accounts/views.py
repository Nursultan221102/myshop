from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from .forms import CustomUserCreationForm
from .models import CustomUser
from shop.models import Product, Promotion, Category

# Create your views here.
class SignUpView(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('shop:home')
    template_name = 'registration/signup.html'


class UserListView(generic.ListView):
    model = CustomUser
    context_object_name = 'users'
    template_name = 'accounts/users_list.html'


class UserPersonalRoom(generic.TemplateView):
    template_name = 'accounts/p_room.html'


class AdminPanelView(generic.ListView):
    model = Product
    template_name = 'accounts/admin.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        context['promos'] = Promotion.objects.all()
        context['categories'] = Category.objects.all()
        return context