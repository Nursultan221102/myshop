from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    birthdate = forms.DateTimeField(label='Birthdate', 
                                    widget=forms.DateTimeInput(attrs={'type':'date'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('gender', 'birthdate')
    